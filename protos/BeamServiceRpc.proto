// SPDX-FileCopyrightText: 2019,2020 Freemelt AB
//
// SPDX-License-Identifier: Apache-2.0

syntax = "proto3";

import "google/protobuf/empty.proto";

package Freemelt.BeamServiceRpc;

service BeamServiceControl {
  // Get the current configuration.
  rpc GetConfiguration(google.protobuf.Empty) returns (Configuration) {}

  // Load compensation maps
  //
  // Loads OBP files which contains compensation maps and prepares to
  // send them. The previous maps are replaced. The maps from the
  // configuration file are sent first.
  rpc LoadCompensation(LoadCompensationRequest) returns (LoadCompensationStatus) {
    // This method has been deprecated because it creates confusion
    // around which compensation maps are active on the machine.
    option deprecated = true;
  }

  // Execute one or several OBP files
  //
  // Streams the requested OBP files to FC.
  rpc StartExposure(StartExposureRequest) returns (StartExposureStatus) {}

  // Stop OBP exposure
  //
  // Stops sending data to FC. This will cause a shutdown of the Beam Service,
  // and in turn the beam (including HV).
  rpc StopExposure(StopExposureRequest) returns (StopExposureStatus) {}

  // Streaming interface to control FC
  //
  // Streams the provided OBP data to FC and manages the queue. Statuses are
  // streamed as the data either completes or is dequeued.
  rpc OpenStream(stream Request) returns (stream Status) {}

  // Request exclusive access to the Beam Service
  //
  // This is an advisory lock mechanism intended for applications that
  // expect to have exclusive control over the beam. Only one stream
  // of this kind can be open at any time, so if your application has
  // the stream open then other applications using this mechanism are
  // guaranteed to not have the stream open.
  rpc ExclusiveAccess(ExclusiveAccessRequest) returns (stream google.protobuf.Empty) {}

  // Request exclusive access to the Beam Service, with acknowledgement
  //
  // This is the same as ExclusiveAccess(), except that it sends a
  // single message on the stream if the request is successful. It
  // allows clients to block on receive in order to be sure that they
  // have exclusive access before they proceed with controlling the
  // beam.
  rpc ExclusiveAccessWithAck(ExclusiveAccessRequest) returns (stream google.protobuf.Empty) {}

  // Store new compensation maps
  //
  // Accepts an OBP file which contains calibration maps and stores
  // them as the currently active calibration. The new calibration is
  // stored on disk and is persistent across restarts.
  rpc StoreCalibrationMaps(StoreCalibrationMapsRequest) returns (StoreCalibrationMapsStatus) {
  }
}

message LoadCompensationRequest {
  message File {
    // Local filename of the compensation map OBP file (must exist on
    // the computer that runs the beam service).
    string obp_file = 1 [deprecated=true];
  }
  repeated File entries = 1 [deprecated=true];
}

message LoadCompensationStatus {
}

message StartExposureRequest {
  message File {
    // Local filename of the OBP file (must exist on the computer that
    // runs the beam service).
    string obp_file = 1;
    // Number of times to expose the file.
    uint32 repetitions = 2;
  }
  repeated File entries = 1;

  // Set this to true to clear the queue
  bool clear_queue = 3;

  // Local filename of an OBP file which will be streamed if the idle
  // scan was streaming immediately prior to this command.
  string post_idle_obp_file = 4;
}

message StartExposureStatus {
}

message StopExposureRequest {
  enum Urgency {
    WHEN_DONE = 0;
    NEXT_FILE = 1;
    NEXT_PACKET = 2;
    IMMEDIATELY = 3;
  }
  Urgency urgency = 1;
}

message StopExposureStatus {
}

message Request {
  // The id will be used in the response (0 = only error responses)
  int32 id = 1;
  // Local filename of the OBP file
  string obp_file = 2;
  // Raw OBP data which, if provided, is used instead of the file
  bytes obp_data = 3;
  // Number of times to expose the data
  uint32 repetitions = 4;
  // Clear the queue immediately before enqueuing this data
  bool clear_queue = 5;
  // Signal the start of the exposure
  bool start_exposure = 6;
  // Local filename of an OBP file which will be streamed if the idle
  // scan was streaming immediately prior to this command.
  string post_idle_obp_file = 7;
  // Stream the latest compensation maps before streaming OBP data for
  // this request. This is normally not needed, and is provided for
  // special workflows where maps are replaced dynamically.
  bool reload_compensation_maps = 8;
}

message Status {
  // The id used in the request.
  int32 id = 1;
  enum ExposureStatus {
    RECEIVED = 0;   // the request has been received
    COMPLETED = 1;  // data has been written to fieldconstruct
    STOPPED = 2;    // data was removed from the queue by another request
    ERRORED = 3;    // the request was invalid (message will be set)
    EXPOSING = 4;   // data is now being exposed at the beam
    EXPOSED = 5;    // the exposure is done
  }
  ExposureStatus status = 2;
  string message = 3;
  int32 fc_frame_id = 4; // current FC frame ID
}

message ExclusiveAccessRequest {
  // The owner of the exclusive access. This field is not interpreted
  // by the software.
  string owner_name = 1;
  // Clear the queue when the exclusive access is lost.
  bool clear_queue = 2;
  // If this is true then the compensation maps will be reloaded when
  // the exclusive access is lost.
  bool reload_compensation_maps = 3;
}

message Configuration {
  message Endpoint {
    string name = 1;
    repeated int32 lines = 2;
  }
  bytes calibration_data = 1;
  float radius_voltage_factor = 2;
  float workspace_rotation = 3;
  float workspace_diameter = 4;
  float default_static_focus_voltage = 5;
  float beam_power_divisor = 6;
  int64 sample_rate = 7;
  repeated Endpoint endpoints = 8;
}

message StoreCalibrationMapsRequest {
  // Suggested filename
  string filename = 1;
  // OBP data with calibration maps
  bytes calibration_data = 2;
  // True if the maps should be made persistent.
  bool persist = 3;
}

message StoreCalibrationMapsStatus {
  // True if the maps were stored, are used as the active maps and
  // will persist across reboots. If false then the maps were just
  // stored in the calibration folder.
  bool persistent = 1;
}
