<!--
SPDX-FileCopyrightText: 2019,2020 Freemelt AB

SPDX-License-Identifier: Apache-2.0
-->

# Freemelt ONE API descriptions
.proto files that describes the interfaces of the various services and controllers in Freemelt ONE software suite. 


# License
Copyright © 2019, 2020 Freemelt AB <opensource@freemelt.com>
The FreemeltAPI is under [Apache License, Version 2.0](http://www.apache.org/licenses/LICENSE-2.0)

